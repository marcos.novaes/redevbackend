const express = require('express')
var bodyParser = require('body-parser')
var cors = require('cors')
const connectDB = require('./config/db');
const config = require('config');
const db = config.get('mongoTestURI');

const app = express()
const PORT = process.env.PORT || 3001;

// Init Middleware
app.use(cors())
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Connect Database

// connectDB(db)
connectDB()

app.get('/', (req, res) => res.send('Hello!'))
app.use('/users', require('./routes/api/users'))
app.use('/topics', require('./routes/api/topic'))
app.use('/login', require('./routes/api/auth'))

const server = app.listen(PORT, () => { console.log(`port ${PORT}`) })


module.exports = { app, server }