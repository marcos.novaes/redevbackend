const express = require('express')
const router = express.Router();
const User = require('../../models/user')
const bcrypt = require('bcryptjs')
const MSGS = require('../../messages')
const auth = require('../../middleware/auth')
const { body, validationResult } = require('express-validator')



// @route    GET /users
// @desc     LIST users
// @access   Public
router.get('/', auth, async (req, res, next) => {
  try {
    const users = await User.find();
    if (users.length > 0) {
      res.status(200).send(users)
    }
    else {
      res.status(404).send({ "erro": MSGS.GENERIC404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": 'Erro!' })
  }
})


// @route    POST /users
// @desc     create user
// @access   Public
router.post('/', [
  body('name').exists().withMessage(MSGS.USER_NAME_REQUIRED),
  body('username').exists().withMessage(MSGS.USERNAME_REQUIRED),
  body('email').isEmail().withMessage(MSGS.VALID_EMAIL),
  body('place').exists().withMessage(MSGS.PLACE_REQUIRED),
  body('gender').isIn(['Male', 'Female', 'Other']).withMessage("Genero tem que ser male, female, ou other"),
  body('password').isLength({ min: 6 }).withMessage(MSGS.PASSWORD_VALIDATED)
], async (req, res) => {
  try {
    let password = req.body.password
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    } else {
      let usuario = new User(req.body)
      const salt = await bcrypt.genSalt(10);
      usuario.password = await bcrypt.hash(password, salt);
      await usuario.save()
      if (usuario.id) {
        res.status(201).json(usuario);
      }
    }

  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": 'Erro!' })
  }
})


// @route    GET /users/:id
// @desc     get one users
// @access   Public
router.get('/:id', auth, async (req, res, next) => {
  try {
    const id = req.params.id
    const user = await User.findById(id)
    if (user) {
      res.json(user)
    } else {
      res.status(404).send({ "error": MSGS.GENERIC404 })
    }

  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": 'Erro!' })
  }
})


router.patch('/profile', auth, async (req, res) => {
  try {
    let user_id = req.user.id
    let bodyRequest = request.body
    const update = { $set: bodyRequest }
    const user = await User.findByIdAndUpdate(user_id, update, { new: true })
    if (user) {
      res.send(user)
    } else {
      res.status(404).send({ error: MSGS.USER404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": 'Erro!' })
  }
})




module.exports = router;