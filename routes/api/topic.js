const express = require('express')
const router = express.Router();
const Topic = require('../../models/topic')
const User = require('../../models/user')
const MSGS = require('../../messages')
const auth = require('../../middleware/auth')
const { body, validationResult } = require('express-validator')

// @route    GET /topic
// @desc     LIST topics
// @access   Public
router.get('/', async (req, res, next) => {
    try {
        const topics = await Topic.find();
        if (topics.length > 0) {
            res.status(200).send(topics)
        }
        else {
            res.status(404).send({ "erro": MSGS.GENERIC404 })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro!' })
    }
})


// @route    GET /topic
// @desc     LIST topics
// @access   Public
router.post('/', [
    body('title').exists().withMessage("Titulo é necessario"),
    body('description').exists().withMessage("Descrição é necessária"),
], auth, async (req, res, next) => {
    try {
        const { title, description } = req.body
        const user = req.user
        const topic = new Topic({ title, description })
        topic.moderators.push(user.id)
        topic.participants_count += 1
        await topic.save()
        if (topic.id) {
            let userToCatch = await User.findById(user.id)
            userToCatch.topics_followed.push(topic.id)
            await userToCatch.save()
            res.status(201).send(topic)
        } else {
            res.status(400).send({ "erro": MSGS.GENERIC_ERROR })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro!' })
    }
})




module.exports = router;