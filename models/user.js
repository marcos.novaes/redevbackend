const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  gender: {
    type: String,
    enum: ['Male', 'Female', 'Other'],
    required: true
  },
  place: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  bio: {
    type: String
  },
  social_links: {
    facebook: String,
    twitter: String,
    github: String,
    linkedin: String
  },
  topics_followed: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'topic'
  }],
  account_confirmed: {
    type: Boolean,
    default: false
  },
  privacy_term_accepted: {
    type: Boolean,
    default: false
  },
  profile_filled: {
    type: Boolean,
    default: false
  }

}, { autoCreate: true })

module.exports = mongoose.model('user', UserSchema);
