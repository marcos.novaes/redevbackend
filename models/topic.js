const mongoose = require('mongoose');



const TopicSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    moderators: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    }],
    participants_count: {
        type: Number,
        default: 0
    },
    created_at: {
        type: Date,
        default: Date.now()
    }
}, { autoCreate: true })

module.exports = mongoose.model('topic', TopicSchema);